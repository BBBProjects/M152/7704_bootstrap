let field = $("#cr-game");
let player = $("#player");
let score = $("#score");
let leftField = $("#cr-left");
let rightField = $("#cr-right");
let cars = [];

let fps = 60;
let mouse = {x: 50, y: 50};

score.score = 0;
player.halfWidth = player.width() / 2;
player.halfHeight = player.height() / 2;

function updateData() {
    field.left = field.offset().left;
    field.right = field.offset().left + field.width();
    field.top = field.offset().top;
    field.bottom = field.offset().top + field.height()
}

function update() {
    updateData();
    scored();
    for (let i = 0; cars.length > i; i++) {
        let car = cars[i];
        //TODO: move cars between their assigned x,y
        if (car.py < field.top) car.up = false;
        if (car.py > field.bottom - player.height()) car.up = true;

        if (car.up == false) {
            car.py += car.speed * score.score;
        } else {
            car.py -= car.speed * score.score;
        }

        car.divE.css({
            left: car.px + field.left + player.halfWidth,
            top: car.py
        });
        // console.log(cars[i].cx);
    }

    $(document).mousemove(function (event) {
        mouse.x = event.pageX;
        mouse.y = event.pageY;
    });
    player.x = mouse.x - player.halfWidth;
    player.y = mouse.y - player.halfHeight;
    keepPlayerInBounds();

    player.css({
        left: player.x,
        top: player.y
    });
    for (let i = 0; i < cars.length; i++) {
        if (collide()) {
            score.text("collision");
        }
    }
}

function scored() {
    if ((score.score % 2) === 0) {
        if (leftField.offset().left + leftField.width() > player.x - player.width())
            scoreChange();
    } else {
        if (rightField.offset().left < player.x + player.width())
            scoreChange();
    }
}

function scoreChange() {
    score.score++;
    score.text("Score: " + score.score);
    switch (score.score) {
        case 0:
            break;
        case 1:
            cars.push(new Car(200, 1));
            break;
        case 5:
            cars.push(new Car(450, 1.2));
            break;
        case 10:
            cars.push(new Car(650, 1.3));
            break;
        case 15:
            cars.push(new Car(900, 1.1));
            break;
    }
}

function collide() {
    for (let i = 0; cars.length > i; i++) {
        let car = cars[i];
        let carOff = car.divE.offset();
        if (!(
            ((carOff.top + car.divE.height()) < player.y) ||
            (carOff.top > (player.y + player.height())) ||
            ((carOff.left + car.divE.width()) < player.x) ||
            (carOff.left > (player.x + player.width()))
        )) return true;
    }
    return false;
}

function keepPlayerInBounds() {
    if (player.x < field.left)
        player.x = field.left;
    if (player.x > field.right - player.width())
        player.x = field.right - player.width();
    if (player.y < field.top)
        player.y = field.top;
    if (player.y > field.bottom - player.height())
        player.y = field.bottom - player.height();
}

$(document).ready(function () {
    setInterval(function () {
        update();
    }, 1000 / fps);
});

class Car {
    constructor(px, speed) {
        this.px = px;
        this.py = 0;
        this.speed = speed;
        this.up = false;

        this.divE = field.append('<div class="car car' + cars.length + '"></div>');
        this.divE = $(".car" + cars.length);
        // console.log("car " + cars.length + " " + fx + " " + tx + " " + this.cx + " " + speed);
        console.log("car " + cars.length + " added, speed " + speed);
    }
}
